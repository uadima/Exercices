import java.util.*;
/**This is class exemaple compiled with javac
*@author Dmitry Bublik
*@version 1.0
*
*
*/
public class HelloWorld{
/** Точка входа в класс и приложение
*@param args Масив строк
*@throws exceptions Исключения не выдаются
*/
public static void main(String[] args){
System.out.println("Hello World");
System.out.println("Сегодня "+new Date());

}
}
/*Output: HelloWorld
Сегодня 09.06.18
*/
