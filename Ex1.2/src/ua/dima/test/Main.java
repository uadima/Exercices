package ua.dima.test;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        List<Integer> number_one = new LinkedList<Integer>();
        List<Integer> number_two = new LinkedList<Integer>();
        for (Integer i = 1; i < 6; i++) {
            number_one.add(new Random().nextInt(10));
            number_two.add(new Random().nextInt(10));
        }
        System.out.println(number_one);
        System.out.println(number_two);
        List<Integer> answer;
        answer=sum(number_one, number_two);
        System.out.println(answer);
            }

    private static List<Integer> sum(List<Integer> number_one, List<Integer> number_two) {
        LinkedList result = new LinkedList<>();
        Integer temp=0;
        for (int a = 0; a < number_one.size(); a++) {
            result.add(a, (number_two.get(a) + number_one.get(a) + temp)%10);
            temp = (number_two.get(a) + number_one.get(a)) /10;
        }
        return result;
    }
}
