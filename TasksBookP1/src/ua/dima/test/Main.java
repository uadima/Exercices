package ua.dima.test;

import static ua.dima.test.Print.print;

public class Main {

    public static void main(String[] args) {
        Operators.post();
        Operators.pref();

    }


}
class Operators{
    static int i = 1;
    public static void pref(){
        System.out.println(++i);
        System.out.println((i==3) ? "true "+i:"false "+i);
    }
    public static void post(){
        System.out.println(i++);
        System.out.println((i==3) ? "true "+i:"false "+i);

    }
}

class P21 implements Cloneable{
     Float f1;
     Float f2;
    static void do_it(){

        P21 p21 =new P21();
        P21 p22 =new P21();
        p21.f2=1.1f;
        p22.f2=2f;
        p22.f1=0.6f;
        System.out.println(p21.f2+" "+p22.f2);
        p21 = p22.clone();
        System.out.println(p21.f2+" "+p22.f2);
        System.out.println(p21.f1+" "+p22.f1);
        p21.f2=0.1f;
        p22.f2=0.2f;
        p21.f1=1.2f;
        p22.f1=1.3f;
        System.out.println(p21.f2+" "+p22.f2);
        System.out.println(p21.f1+" "+p22.f1);

    }
    @Override
    public P21 clone() {
        try {
            return (P21)super.clone();
        }
        catch (CloneNotSupportedException e){
            System.out.println("Clone not supported"+e);
            return null;
        }
    }

}


class P2 {
    static void do_it() {

    }
}

class Incrementable {
    int f;
    boolean bool;
    char aChar;

    static void increment() {
        StaticTest.i++;
        System.out.println(StaticTest.i);
    }
}

class StaticTest {
    static int i = 2;
}

class P1 {
    static StaticTest temp = new StaticTest();

    protected static void do_it() {
        try {
            if (temp.equals(null)) {
                System.out.println("NULL");
            } else {
                temp.i++;
                System.out.println(temp.i);
            }
        }catch (NullPointerException e){
            print("объект null");
            return;
        }

        Incrementable.increment();
        Incrementable.increment();
        Incrementable.increment();
        Incrementable.increment();
        Incrementable incrementable = new Incrementable();
        System.out.println(incrementable.aChar);
        System.getProperties().list(System.out);
        System.out.println(System.getProperty("user.name"));
        System.out.println(System.getProperty("java.library.path"));
        print("test");
    }
}

